# Install NodeJS
選擇你想要安裝的系統平台 : Windows, MAC, Linux  
[https://nodejs.org/en/](https://nodejs.org/en/)  

檢查 nodejs 是否正確安裝，可以使用以下的指令：  
```
node -v
```

# Install npm 套件管理工具
Node.js 在 0.6.3 版本開始內建 npm  
檢查 npm 是否正確安裝，可以使用以下的指令：  
```
npm -v
```

# Install yarn 套管理工具
Yarn 是 Facebook 自家團隊與 Exponent、 Google、Tilde 所合作開發的套件管理工具，由於程式套件隨著團隊的規模茁壯，他們在安全性和效能面臨一大考驗，所以他們團隊自己打造全新的解決方案，以一種更加可靠的方式來管理依賴，Yarn 因此就誕生了，它作為 npm 客戶端的替代器，更加快速、可靠、安全。

除了安裝變得更加快速和可靠以外，Yarn 還提了如下特性，進一步簡化了依賴管理的工作流程：
- 同时兼容 npm 和 Bower 工作流，支持混用多種註冊表類型。
- 可以限制依赖包的授權類型，並且可以輸出依赖包的授權訊息。
- 暴露一個稳定的 JS API，提供抽象化的日誌訊息(log)在於編譯環境。
- 提供可讀的、最小化的、美觀的 CLI (英：command-line interface ， 中：命令介面) 輸出信息。


Windows安裝
```
npm install yarn --g
```

Mac 安裝
```
brew install yarn

or

npm install yarn --g
```

確認是否安裝成功
```
yarn --version
```

## npm 與 Yarn 指令比較

  比較常用指令
    NPM                                | YARN                      | DESC
    -----------------------------------|---------------------------|----------------------------------
    npm install                        | yarn install              | 安裝 json.package 所有依賴
    npm install [package]              | (N/A)                     | Yarn不支援直接安裝套件
    npm install --save [package]       | yarn add [paakage]        | 儲存在 json.package中的dependencies
    npm install --save-dev [package]   | yarn add [paakage] --dev  | 儲存在 json.package中的devDependencies
    npm install --global [package]     | yarn global add [package] | 安裝在電腦全域中
    npm uninstall [package]            | (N/A)                     | Yarn不支援直接安裝與移除套件
    npm uninstall --save [package]     | yarn remove [package]     | 移除dependencies某套件
    npm uninstall --save-dev [package] | yarn remove [package]     | 移除devDependencies某套件
    rm -rf node_modules && npm install | yarn upgrade              | 更新node_modules

- 全部yarn指令
  - [https://yarnpkg.com/en/docs/migrating-from-npm](https://yarnpkg.com/en/docs/migrating-from-npm)

# 安裝 Express-Generator
Express 可以說是 Node.js 底下的一個前端 + 後端的框架，也是被官方所認同推薦入門的套件之一，其中包含 MVC Framework

```
npm install -g express-generator
```

確認是否有安裝成功
```
express --version
```

使用express-generator建立node express專案
```
express -f [專案名稱]
```


# 各作業系統更新node, npm, yarn...等套件方式

## MAC
```
brew update
brew upgrade node
npm update -g npm
brew upgrade yarn
```
> yarn可以透過npm安裝更新

## Windows
 - 透過官網安裝檔更新node, npm
 - 再透過npm安裝yarn

## Ubuntu
https://websiteforstudents.com/install-the-latest-node-js-and-nmp-packages-on-ubuntu-16-04-18-04-lts/

```sh
sudo apt-get install curl

# 註：11.x表示安裝nodejs 11最新版本, 你也可以改成10.x, 9.x ...
curl -sL https://deb.nodesource.com/setup_11.x | sudo bash -

# 安裝node, npm
sudo apt-get install nodejs

# 安裝yarn
sudo npm install -g yarn
```

## npm移除套件方法
參考 : https://docs.npmjs.com/uninstalling-packages-and-dependencies

Unscoped package
```
npm uninstall -g <package_name>
```

Scoped package
```
npm uninstall -g <@scope/package_name>
```

# npm 與 yarn速度比較
參考：[用 Yarn 取代 Npm 管理 JavaScript 套件版本](https://blog.wu-boy.com/2016/10/replcae-npm-with-yarn-package-management/)


# package json 屬性詳細說明
https://docs.npmjs.com/files/package.json#main
https://www.cnblogs.com/tzyy/p/5193811.html#_h1_10
http://javascript.ruanyifeng.com/nodejs/packagejson.html#toc0