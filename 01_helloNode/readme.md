# Run NodeJS

```sh
node index.js
```

# 初始化nodejs專案

```
npm init -y
```
初始完成後你會發現資料夾多了一個叫 package.json 的檔案，它是一種 `CommonJS` 規定用來描述包的文件，是一個包含 json 格式的說明文件，裡面可以定義相依的相關套件以及應用程式的資訊。



# package json 屬性詳細說明
https://www.cnblogs.com/tzyy/p/5193811.html#_h1_10