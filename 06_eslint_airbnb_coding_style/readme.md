
# Airbnb+ JavaScript Standard Style
https://www.npmjs.com/package/eslint-config-airbnb-standard


Install it locally :
```sh
yarn add -D eslint-config-airbnb-standard
```

Check install completely
```sh
$ node_modules/.bin/eslint -v
v5.13.0

$ node_modules/eslint-config-airbnb-standard/node_modules/.bin/eslint -v
v5.16.0
```

# Setup your IDE / Editor:
- WebStorm
- [VSCode](https://www.npmjs.com/package/eslint-config-airbnb-standard#user-content-vscode)
- Sublime Text 3
- Atom


# VSCode (windows)

add this to `settings.json` on vscode setting
```json
{
  "eslint.nodePath": "node_modules/eslint-config-airbnb-standard/node_modules/.bin/eslint",
}
```

Create `.eslintrc.js` file inside your working project root:
```js
process.chdir(__dirname);
 
module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    allowImportExportEverywhere: true,
    codeFrame: false
  },
  extends: [
    'airbnb-standard',
  ],
  rules: {
    'no-console': 'off', // 若不想看到 Unexpected console statement 的警告訊息，可以加這個 rule
  },
};
```

# Reference
- [Airbnb JavaScript Style Guide](https://github.com/jigsawye/javascript)
  - 彙整 JS 普遍的程式撰寫風格 (中文) 
