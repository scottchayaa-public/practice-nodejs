# Create Express Project
```sh
# install express-generater
express --no-view 02_helloExpress
```

# Install package
```
npm install

or 

yarn install
```

# Add /demo route
routes/index.js
```js
router.get('/demo', function(req, res, next) {
  res.send('This is Demo')
});
```

# Run server
```
node ./bin/www

or

npm start
```

# Check result

- [http://localhost:3000](http://localhost:3000)
- [http://localhost:3000/demo](http://localhost:3000/demo)