# Simple RESTful API Start Kit 2017

這包環境可以讓你快速開發一個具有 REST 風格的 API 內部包含 Moudle 和 Controller (未包括 View)，提供一個輕巧快速的 Start Kit，並有詳細的套件解說，讓你了解內部架構。

# Migration




## Feature 功能
- ESLint 使用 Airbnb 寫作風格
  - [Link 連結](https://github.com/andy6804tw/RESTful_API_start_kit/tree/Part1-ESLint)
  - [Tutorial 教學](/tutorials/Part1-ESLint.md)
- Webpack & Babel 自動化編譯
  - [Link 連結](https://github.com/andy6804tw/RESTful_API_start_kit/tree/Part2-Webpack%26Babel)
  - [Tutorial 教學](/tutorials/Part2-Webpack&Babel.md)
- Express安裝與設定路由
  - [Link 連結](https://github.com/andy6804tw/RESTful_API_start_kit/tree/Part3-Express)
  - [Tutorial 教學](/tutorials/Part3-Express.md)
- middleware 常用安裝
  - [Link 連結](https://github.com/andy6804tw/RESTful_API_start_kit/tree/Part4-middleware)
  - [Tutorial 教學](/tutorials/Part4-middleware.md)



# package dependencies description
- "dotenv": 將 .env 文件中的環境參數加載到 process.env，在其他文件中先引入 require('dotenv').config() 後只要再呼叫 PROCESS.ENV.[變數名稱] 就能將此環境參數撈出來了，通常還會搭配 `joi` 來做設定
- "express": Simple node web framwork
- "joi": 規範 schema 來限制資料格式
- "nodemon" 自動reload變更的js代碼,  取代 node 執行你的程式碼
- "morgan": HTTP request logger middleware for node.js
- "bycrypt": bcrypt 能夠將一個字串做雜湊加密，寫入資料庫前要先將用戶密碼加密才是正確的做法
- "express-validation": express-validation is a middleware that validates the body, params, query, headers and cookies of a request and returns a response with errors; if any of the configured validation rules fail.
- httpStatus : http status code package
- babel : Babel 的作用就是將你的程式轉換為各種瀏覽器看的懂的語法