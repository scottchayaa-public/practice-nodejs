// article.module.js
import mysql from 'mysql';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import config from '../../config/config';
import AppError from '../helper/AppError';

const connectionPool = mysql.createPool({
    connectionLimit: 10,
    host: config.mysqlHost,
    user: config.mysqlUserName,
    password: config.mysqlPass,
    database: config.mysqlDatabase
});

/** Create new data */
const create = (insertValues) => {
    return new Promise((resolve, reject) => {
        connectionPool.getConnection((connectionError, connection) => { // 資料庫連線
            if (connectionError) {
                reject(connectionError); // 若連線有問題回傳錯誤
            } else {
                connection.query('INSERT INTO users SET ?', insertValues, (error, result) => { // Article資料表寫入一筆資料
                    if (error) {
                        console.error('SQL error: ', error); // 寫入資料庫有問題時回傳錯誤
                        reject(error);
                    } else if (result.affectedRows === 1) {
                        resolve(`Successfully add user data, id: ${result.insertId}`); // 寫入成功回傳寫入id
                    }
                    connection.release();
                });
            }
        });
    });
};

/** Get all data */
const getAll = () => {
    return new Promise((resolve, reject) => {
        connectionPool.getConnection((connectionError, connection) => {
            if (connectionError) {
                reject(connectionError);
            } else {
                connection.query('SELECT * FROM users', (error, result) => {
                    if (error) {
                        console.error('SQL error: ', error);
                        reject(error);
                    } else {
                        console.log(result);
                        resolve(result);
                    }
                    connection.release();
                });
            }
        });
    });
};

/** Update data */
const update = (insertValues, userId) => {
    return new Promise((resolve, reject) => {
        connectionPool.getConnection((connectionError, connection) => {
            if (connectionError) {
                reject(connectionError);
            } else {
                connection.query('UPDATE users SET ? WHERE id = ?', [insertValues, userId], (error, result) => {
                    if (error) {
                        console.error('SQL error: ', error);
                        reject(error);
                    } else if (result.affectedRows === 0) { // 寫入時發現無該筆資料
                        resolve('userId is incorrect');
                    } else if (result.message.match('Changed: 1')) { // 寫入成功
                        resolve('Successfully update user data');
                    } else {
                        resolve('Data no change'); // 資料無異動
                    }
                    connection.release();
                });
            }
        });
    });
};

/** Delete data */
const destroy = (userId) => {
    return new Promise((resolve, reject) => {
        connectionPool.getConnection((connectionError, connection) => {
            if (connectionError) {
                reject(connectionError);
            } else {
                connection.query('DELETE FROM users WHERE id = ?', userId, (error, result) => {
                    if (error) {
                        console.error('SQL error: ', error);
                        reject(error);
                    } else if (result.affectedRows === 1) {
                        resolve('Successfully delete user data !');
                    } else {
                        resolve('Failed to delete user data');
                    }
                    connection.release();
                });
            }
        });
    });
};

/** Login */
const login = (insertValues) => {
    return new Promise((resolve, reject) => {
        connectionPool.getConnection((connectionError, connection) => {
            if (connectionError) {
                reject(connectionError);
            } else {
                connection.query('SELECT * FROM users WHERE email = ?', insertValues.email, (error, result) => {
                    if (error) {
                        console.error('SQL error: ', error);
                        reject(error);
                    } else if (Object.keys(result).length === 0) {
                        reject(new AppError.LoginError1());
                    } else {
                        const dbHashPassword = result[0].password; // 資料庫加密後的密碼
                        const userPassword = insertValues.password; // 使用者登入輸入的密碼
                        bcrypt.compare(userPassword, dbHashPassword).then((res) => { // 使用bcrypt做解密驗證
                            if (res) {
                                const user = {
                                    id: result[0].id,
                                    name: result[0].name,
                                    email: result[0].email
                                };
                                // 取得 API Token
                                const token = jwt.sign({ user, exp: Math.floor(Date.now() / 1000) + (60 * config.jwtEXP) }, config.jwtSecret);
                                resolve(Object.assign({ code: 200 }, { message: '登入成功', token })); // 登入成功
                            } else {
                                reject(new AppError.LoginError2());
                            }
                        });
                    }
                    connection.release();
                });
            }
        });
    });
};

export default {
    create,
    getAll,
    update,
    destroy,
    login
};
