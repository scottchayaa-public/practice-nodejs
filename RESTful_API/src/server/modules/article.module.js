// article.module.js
import mysql from 'mysql';
import jwt from 'jsonwebtoken';
import config from '../../config/config';

const connectionPool = mysql.createPool({
    connectionLimit: 10,
    host: config.mysqlHost,
    user: config.mysqlUserName,
    password: config.mysqlPass,
    database: config.mysqlDatabase
});

/** Create new data */
const create = (insertValues) => {
    return new Promise((resolve, reject) => {
        connectionPool.getConnection((connectionError, connection) => { // 資料庫連線
            if (connectionError) {
                reject(connectionError); // 若連線有問題回傳錯誤
            } else {
                connection.query('INSERT INTO articles SET ?', insertValues, (error, result) => { // Article資料表寫入一筆資料
                    if (error) {
                        console.error('SQL error: ', error); // 寫入資料庫有問題時回傳錯誤
                        reject(error);
                    } else if (result.affectedRows === 1) {
                        resolve(`Successfully add article data, id: ${result.insertId}`); // 寫入成功回傳寫入id
                    }
                    connection.release();
                });
            }
        });
    });
};

/** Get all data */
const getAll = () => {
    return new Promise((resolve, reject) => {
        connectionPool.getConnection((connectionError, connection) => { // 資料庫連線
            if (connectionError) {
                reject(connectionError);
            } else {
                connection.query('SELECT * FROM articles', (error, result) => {
                    if (error) {
                        console.error('SQL error: ', error);
                        reject(error); // 寫入資料庫有問題時回傳錯誤
                    } else {
                        console.log(result);
                        resolve(result); // 撈取成功回傳 JSON 資料
                    }
                    connection.release();
                });
            }
        });
    });
};

/*  Article GET JWT取得個人文章  */
const personalArticle = (token) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, config.jwtSecret, (err, payload) => {
            if (err) {
                reject(err); // 驗證失敗回傳錯誤
            } else {
                // JWT 驗證成功 ->取得user id
                const userId = payload.user.id;
                // JWT 驗證成功 -> 撈取該使用者的所有文章
                connectionPool.getConnection((connectionError, connection) => { // 資料庫連線
                    if (connectionError) {
                        reject(connectionError); // 若連線有問題回傳錯誤
                    } else {
                        connection.query(
                            'SELECT * FROM articles WHERE user_id = ?', [userId]
                            , (error, result) => {
                                if (error) {
                                    reject(error); // 寫入資料庫有問題時回傳錯誤
                                } else {
                                    console.log(result);
                                    resolve(result); // 撈取成功回傳 JSON 資料
                                }
                                connection.release();
                            }
                        );
                    }
                });

                //console.log(payload.user.id);
                //resolve(payload); // 驗證成功回傳 payload data
            }
        });
    });
};

/** Update data */
const update = (insertValues, articleId) => {
    return new Promise((resolve, reject) => {
        connectionPool.getConnection((connectionError, connection) => {
            if (connectionError) {
                reject(connectionError);
            } else {
                connection.query('UPDATE articles SET ? WHERE id = ?', [insertValues, articleId], (error, result) => {
                    if (error) {
                        console.error('SQL error: ', error);
                        reject(error);
                    } else if (result.affectedRows === 0) { // 寫入時發現無該筆資料
                        resolve('articleId is incorrect');
                    } else if (result.message.match('Changed: 1')) { // 寫入成功
                        resolve('Successfully update article data');
                    } else {
                        resolve('Data no change'); // 資料無異動
                    }
                    connection.release();
                });
            }
        });
    });
};

/** Delete data */
const destroy = (articleId) => {
    return new Promise((resolve, reject) => {
        connectionPool.getConnection((connectionError, connection) => {
            if (connectionError) {
                reject(connectionError);
            } else {
                connection.query('DELETE FROM articles WHERE id = ?', articleId, (error, result) => {
                    if (error) {
                        console.error('SQL error: ', error);
                        reject(error);
                    } else if (result.affectedRows === 1) {
                        resolve('Successfully delete article data !');
                    } else {
                        resolve('Failed to delete article data');
                    }
                    connection.release();
                });
            }
        });
    });
};

export default {
    create,
    getAll,
    update,
    destroy,
    personalArticle
};
