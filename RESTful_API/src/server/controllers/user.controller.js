import bcrypt from 'bcrypt';
import UserModule from '../modules/user.module';

/**
 * Add new user data
 * @param {JSON} req json input user table data
 * @param {JSON} res json response
 */
const create = (req, res) => {
    // 取得新增參數
    // const insertValues = req.body;

    const insertValues = {
        name: req.body.name,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10) // 密碼加密
    };

    UserModule.create(insertValues).then((result) => {
        return res.send(result); // 成功回傳result結果
    }).catch((err) => {
        return res.send(err); // 失敗回傳錯誤訊息
    });
};

/** Get all user data  */
const getAll = (req, res) => {
    UserModule.getAll().then((result) => {
        res.send(result); // 成功回傳result結果
    }).catch((err) => { return res.send(err); }); // 失敗回傳錯誤訊息
};

/** Update user data */
const update = (req, res) => {
    // 取得url params : user_id
    const userId = req.params.user_id;
    const insertValues = req.body;

    UserModule.update(insertValues, userId).then((result) => {
        res.send(result);
    }).catch((err) => { return res.send(err); });
};

/** Delete user data */
const destory = (req, res) => {
    // 取得url params : user_id
    const userId = req.params.user_id;

    UserModule.destroy(userId).then((result) => {
        res.send(result);
    }).catch((err) => { return res.send(err); });
};

/** User Login */
const login = (req, res, next) => {
    const insertValues = req.body;
    UserModule.login(insertValues).then((result) => {
        res.send(result);
    }).catch((err) => { next(err); });
};

export default {
    create,
    getAll,
    update,
    destory,
    login
};
