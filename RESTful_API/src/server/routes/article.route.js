// article.route.js
import express from 'express';
import validate from 'express-validation';
import ArticleController from '../controllers/article.controller';
import paramValidation from '../../config/param-validation';
import jwtAuth from '../middleware/jwt.auth';

const router = express.Router();

router.route('/').get(ArticleController.getAll);
router.route('/').post(validate(paramValidation.createArticle), ArticleController.create);
router.route('/:article_id').put(ArticleController.update);
router.route('/:article_id').delete(ArticleController.destory);

/** 取得某用戶 Article 所有值組 */
router.get('/personal', jwtAuth.ensureToken, ArticleController.getPersonalArticle);


export default router;
