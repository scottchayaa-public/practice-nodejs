# Introduction

This is a sample of calling `commander` library without using shell.
We run it as `function` or `library` mode.

Then we can pass `array argv` to our customized commander function
