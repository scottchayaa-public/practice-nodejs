
const program = require('commander');
const commandName = 'example';

program
  .name(commandName)
  .version('0.0.1');

program
  .command('hello')
  .description('I will reply garbage words to you')
  .action(function () {
    console.log('Hello world....foo');
  });

program
  .command('rm <dir>')
  .option('-r, --recursive', 'Remove recursively')
  .action(function (dir, cmd) {
    console.log('remove ' + dir + (cmd.recursive ? ' recursively' : ''))
  });

module.exports.run = function (argv) {
  let fake_argv = ['', '', ...argv];
  program.parse(fake_argv);
}