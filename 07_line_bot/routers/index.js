const router = require('express').Router();
const request = require('request');
const config = require('../config');

router.post('/webhook', function (req, res) {
  let allReply = [];

  req.body.events.forEach(event => {
    allReply.push(autoReply(event));
  });

  Promise.all(allReply).then(resopnse => {
    res.json(resopnse);
  });
});

router.post('/push', function (req, res) {
  push(req.body.to, req.body.messages)
  .then(response => {
    res.json(response);
  })
  .catch(err => {
    res.json(err);
  });
});

function autoReply(event) {
  let apiUrl = 'https://api.line.me/v2/bot/message/reply';

  return new Promise(function (resolve, reject) {
    request.post({
      url: apiUrl,
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${config.line.message.token}`
      },
      json: {
        replyToken: event.replyToken,
        messages: [
          { type: "text", text: "hihi" }
        ]
      }
    }, function (err, res, body) {
      if (err) {
        reject(err);
      }
      resolve(body);
    });
  });
}

function push(to, messages) {
  let apiUrl = 'https://api.line.me/v2/bot/message/push';

  return new Promise(function (resolve, reject) {
    request.post({
      url: apiUrl,
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${config.line.message.token}`
      },
      json: {
        to: to,
        messages: messages
      }
    }, function (err, res, body) {
      if (err) {
        reject(err);
      }
      resolve(body);
    });
  });
}


module.exports = router;
