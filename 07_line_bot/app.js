const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');
const morganBody = require('morgan-body');
const uuid = require('node-uuid')

const app = express();

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(function (req, res, next) {
    req.id = uuid.v4()
    next()
});

// 攔截 response body 並暫存到 res.__body_response
// app.use(function (req, res, next) {
//   var originalSend = res.send;
//   res.send = function (body) { // res.send() 和 res.json() 都會攔截到
//     res.__body_response = body;
//     originalSend.call(this, body);
//   }
//   next();
// });

// 自訂錯誤訊息
// app.use(morgan(function (tokens, req, res) {
//   return [
//     tokens.method(req, res),
//     tokens.url(req, res),
//     tokens.status(req, res),
//     '-',
//     tokens['response-time'](req, res),
//     'ms',
//     '\nrequest: ' + JSON.stringify(req.body),
//     '\nresponse: ' + res.__body_response,
//   ].join(' ')
// }));

// 自訂錯誤訊息: morgan-Body
morganBody(app);


// Routers
app.use('/', require('./routers/index'));


app.listen(3000, function () {
    console.log('Listening on port 3000!');
});

module.exports = app;
