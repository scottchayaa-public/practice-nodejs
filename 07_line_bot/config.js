require('dotenv').config();

const config = {
  line: {
    message: {
      id: process.env.LINE_MESSAGE_ID,
      secret: process.env.LINE_MESSAGE_SECRET,
      token: process.env.LINE_MESSAGE_TOKEN
    },
    
    login: {
      id: process.env.LINE_LOGIN_ID,
      secret: process.env.LINE_LOGIN_SECRET
    },
  }
};

module.exports = config;