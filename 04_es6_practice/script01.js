/**
 * Javascript基礎篇 
 * 新的特性和用法學習
 * 整理簡單可以執行的sample code
 * vscode 可以直接按F5執行測試
 */

// let有區域變數的概念, 如果超出for迴圈則會讀不到 i 值
for (let i = 0; i < 10; i += 1) {
    console.log(`loop is run ${i} times`);
}

// 讀取顯示陣列
let mArray = ['aa', 2, 'bb']
for (let value of mArray) {
    console.log(value); // 1 2 3
}

// 箭頭函式(Arrow function)
const sum = (x, y) => {
    return x + y;
};
console.log(sum(1, 3));

const squareNum = (num) => {
    return num * num;
};
console.log(squareNum(5));

/**
 * ES6的特性: 展開(spread)運算子, 使用...代表陣列
 * 當不知道引數有多少時可以很方便使用
 */
const call = (...arr) => {
    console.log(arr); // [ 1, 2, 3 ]
};
call(1, 2, 3);

/**
 * Dog物件
 * 列出Dog Class裡面所有屬性值
 */
const dog = {
    name: 'Tom',
    breed: 'Golden Retriever',
    weight: 60,
    breaks() {
        console.log('woof');
    }
};
Object.keys(dog).forEach((key) => {
    console.log(dog[key]);
});
dog.breaks();


/**
 * 物件展開用法
 * 有點像是物件的繼承
 */
const person = {
    name: 'Andy',
    hello() {
        console.log(this.name);
    }
};
const newPerson = {
    ...person,
    name: 'Mary',
    age: 21
};

newPerson.hello();
console.log(newPerson);

/**
 * Destructuring Assignment (解構賦值)
 * Easily extract array elements or object properties and store them in variables
 * 解構解構允許你拉出單個元素或屬性，並將他們儲存在數組的變數中
 */
const numbers = [1, 2, 3];
const [num1, num2] = numbers;
console.log(num1, num2);

/**
 * Class 繼承寫法
 */
class Human {
    constructor() {
        this.gender = 'male';
    }
}

class Person extends Human {
    constructor() {
        super();
        this.name = 'Andy';
    }
    call() {
        console.log(this.name);
        console.log(this.gender);
    }
}

const myPerson = new Person();

myPerson.call();

