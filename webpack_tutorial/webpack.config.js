/* webpack.config.js ： Webpack 的設定檔 */

const nodeExternals = require('webpack-node-externals');
const path = require('path');

module.exports = {
    target: 'node',
    externals: [nodeExternals()],
    entry: {    //程式進入點
        'index': './src/index.js',
    },
    output: {
        //打包後的路徑，這邊設定會產出一個 dist 資料夾，打包後的東西就儲存在這裡
        path: path.join(__dirname, 'dist'),
        //打包後的檔名, [name] 來自於 entry 的 index 名字會同步
        filename: '[name].bundle.js',
        //使用 commonjs2 的解析，bundle 最終會以 module.exports 導出整個bundle模塊
        libraryTarget: 'commonjs2',
    },
    module: {   //設定你的檔案選項
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
        ],
    },
}