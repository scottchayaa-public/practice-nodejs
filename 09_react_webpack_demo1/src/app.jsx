import React from "react";
import ReactDOM from "react-dom";

class HelloTitle extends React.Component {
  render() {
    // props = Component 裡面的屬性，類似 attributes
    console.log(this.props.style);
    return <p style={this.props.style}>{this.props.content}</p>;
  }
}

class TitleDiv extends React.Component {
  render() {
    return (
      <div>
        <HelloTitle content="比較大的字" style={{ "font-size": 18 }} />
        <HelloTitle content="比較小的字" style={{ "font-size": 12 }} />
      </div>
    );
  }
}

class NowTime extends React.Component {
  // 使用類別中的constructor建構子，參數中傳入props是必要的
  constructor(props) {
    super(props);
    // 狀態 : 在組件中自身不會被修改的屬性
    this.state = { time: new Date().toLocaleTimeString() };
  }

  //加入組件建構完成後執行的事件
  componentDidMount() {
    // 在建構完成後，每秒都去刷新this.state.time的值
    // (1)先去宣告一個更新state內容的function
    // (2)每秒去執行一次該function刷新
    const updateTime = () => {
      this.setState({ time: new Date().toLocaleTimeString() });
    };

    this.timer = setInterval(updateTime, 1000);
  }

  // Component 被修改時會執行的函式
  componentDidUpdate() {
    console.log(this.state.time);
  }

  // Component 結束時會執行的事件
  componentWillUnmount() {
    // 這裡記錄移除掉的時間
    console.log(`移除組件的時間為：${this.state.time}`);
    clearInterval(this.timer);
  }

  render() {
    return <h1>現在時間是 {this.state.time}</h1>;
  }
}

/**
 * Click Event
 */
class CheckButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { clickCount: 0 };
    this.addCount = this.addCount.bind(this);
  }

  componentDidMount() {
    // this 有資料 => 因為 componentDidMount() 是屬於這個 class 本身的方法
    console.log(this);
  }

  addCount(count) {
    // this 無資料 undefined => 因為是自訂方法，需要透過 bind 傳入 this
    // console.log(this);
    this.setState({ clickCount: this.state.clickCount + count });
  }

  componentDidUpdate() {
    console.log(`點了 ${this.state.clickCount} 下`);
  }

  render() {
    //使用onClick指定觸發的事件
    return (
      <input
        type="button"
        onClick={this.addCount.bind(this, 2)}
        value="點我看console"
      />
    );
  }
}

class InputGender extends React.Component {
  constructor(props) {
    super(props);
    this.state = { gender: "" };
    this.changeGender = this.changeGender.bind(this);
  }
  //strA是傳入的參數
  changeGender(strA) {
    console.log(`傳入參數${strA}`);
    console.log(window.event.target);
    this.setState({ gender: event.target.value });
  }
  componentDidUpdate() {
    console.log(`已將state.gender變動為：${this.state.gender}`);
  }
  render() {
    //在onchange中增加一個自訂參數
    return (
      <select onChange={this.changeGender.bind(this, "aaaa")}>
        <option value="M">男</option>
        <option value="W">女</option>
      </select>
    );
  }
}

/**
 * 1
 */
// ReactDOM.render(<TitleDiv />, document.getElementById("root"));

/**
 * 2
 */
// ReactDOM.render(<NowTime />, document.getElementById("root"));
// const removeComponent = () => {
//   ReactDOM.unmountComponentAtNode(document.getElementById("root"));
// };
// setTimeout(removeComponent, 5000);

/**
 * 3
 */
// ReactDOM.render(<CheckButton />, document.getElementById("root"));

/**
 * 4
 */
ReactDOM.render(<InputGender />, document.getElementById("root"));
