//引用path模組
const path = require("path");
module.exports = {
  entry: {
    index: ["./src/index.js", "./src/app.jsx"]
  },
  output: {
    filename: "bundle.js",
    path: path.join(__dirname, "/dist")
  },
  //將loader的設定寫在module的rules屬性中
  module: {
    //rules的值是一個陣列可以存放多個loader物件
    rules: [
      //第一個loader編譯JSX
      {
        test: /.jsx$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: { presets: ["@babel/preset-react", "@babel/preset-env"] }
        }
      },
      //第二個loader編譯ES6 to ES5
      {
        test: /.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: { presets: ["@babel/preset-env"] }
        }
      }
    ]
  },
  //增加一個給devserver的設定
  devServer: {
    contentBase: path.join(__dirname, 'dist'), // index.html 放在 /dist 的話需要設定這行
    compress: true, // 將 index.html 轉換成 index.html.zip, 然後再 client browser 再解開
    port: 9000
  }
};
