

# [從零到一的webpack開發環境(1)-安裝執行篇](https://ithelp.ithome.com.tw/articles/10200329)
```
yarn init -y

yarn add -D webpack webpack-cli

vi webpack.config.js
```

# [從零到一的webpack開發環境(2)-React開發篇](https://ithelp.ithome.com.tw/articles/10200459)
```sh
yarn add react react-dom

yarn add -D babel-loader # 因瀏覽器看不懂 JSX，所以讓 webpack 知道說，什麼樣子的檔案要用什麼 loader 來編譯

yarn add -D @babel/core # babel 版本 8 以上的核心套件 @babel/core 和版本 7 的 babel-core 不同

# 翻譯react語法的preset，針對JSX處理，webpack會在打包的時候依照選擇的preset把檔案編譯成JavaScript
yarn add -D @babel/preset-react

# 翻譯ES6語法的preset
yarn add -D @babel/preset-env

# 開發階段時，可即時產生一個 local server 方便看執行結果
yarn add -D webpack-dev-server
```